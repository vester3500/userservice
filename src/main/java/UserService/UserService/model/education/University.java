package UserService.UserService.model.education;

import UserService.UserService.model.Address;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public final class University  extends  Institution implements Serializable {
    private static final long serialVersionUID = -3540717445598077208L;

    @Column(name = "universityDegree")
    private String universityDegree;

    public University(String name,
                         Boolean graduated,
                         Address address,
                         LocalDateTime dateCreated,
                         String universityDegree

    ) {
        super(name,graduated, address, dateCreated);
        this.universityDegree = universityDegree;
    }
}
