package UserService.UserService.model.education;
import UserService.UserService.model.Address;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.Entity;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")

public final class HighSchool  extends  Institution implements Serializable {
    private static final long serialVersionUID = -7551579633894796426L;

    public HighSchool(String name,
                       Boolean graduated,
                      Address address,
                      LocalDateTime dateCreated
    ) {
        super(name,graduated, address, dateCreated);
    }
}
