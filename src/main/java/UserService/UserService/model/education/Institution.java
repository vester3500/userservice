package UserService.UserService.model.education;
import UserService.UserService.model.Address;
import UserService.UserService.model.enums.Visibility;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")

public  abstract class  Institution implements Serializable {
    @Id
    @Column(updatable = false, nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(nullable = false)
    private String name;

    @Column(name = "start_date")
    private String startDate;

    @Column(name = "end_date")
    private Date endDate;

    private Boolean graduated = false;

    private String description;

    @OneToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(nullable = false)
    private Address address;


    @Column(name = "date_created", updatable = false, nullable = false)
    private LocalDateTime dateCreated = LocalDateTime.now();

    @Enumerated(EnumType.STRING)
    private Visibility visibility;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EDUCATION_ID", updatable = false, nullable = false)
    private Education education;

    public Institution(@NotNull String name, String startDate, Date endDate, Boolean graduated, String description, Address address, LocalDateTime dateCreated, Visibility visibility) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.graduated = graduated == null ? false : graduated;
        this.description = description;
        this.address = address;
        this.dateCreated = dateCreated;
        this.visibility = visibility == null ? Visibility.PUBLIC : visibility;
    }

    public Institution(@NotNull String name, Boolean graduated, Address address, LocalDateTime dateCreated, Visibility visibility) {
        this.name = name;
        this.graduated = graduated == null ? false : graduated;
        this.address = address;
        this.dateCreated = dateCreated;
        this.visibility = visibility == null ? Visibility.PUBLIC : visibility;
    }


    public Institution(String name, Boolean graduated, Address address, LocalDateTime dateCreated) {
        this.name = name;
        this.graduated  = graduated == null ? false : graduated;
        this.dateCreated = dateCreated;
        this.address = address;
    }

}
