package UserService.UserService.model.education;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public final class Education implements Serializable {
    private static final long serialVersionUID = 875119387906276782L;
    @Id
    @Column(updatable = false, nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "Created_date", updatable = false, nullable = false)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime createdDate = LocalDateTime.now();

    @JsonManagedReference
    @Column(updatable = false)
    @OneToMany(mappedBy = "education", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<PrimarySchool> primarySchools = new HashSet<>();

    @JsonManagedReference
    @Column(updatable = false)
    @OneToMany(mappedBy = "education", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<HighSchool> highSchools = new HashSet<>();

    @JsonManagedReference
    @Column(updatable = false)
    @OneToMany(mappedBy = "education", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<University> universities = new HashSet<>();

    public void setPrimarySchools(Set<PrimarySchool> primarySchools) {
        primarySchools.forEach(primarySchool -> primarySchool.setEducation(this));
        this.primarySchools = primarySchools;
    }

    public void setHighSchools(Set<HighSchool> highSchools) {
        highSchools.forEach(highSchool -> highSchool.setEducation(this));
        this.highSchools = highSchools;
    }

    public void setUniversities(Set<University> universities) {
        universities.forEach(university -> university.setEducation(this));
        this.universities = universities;
    }

}
