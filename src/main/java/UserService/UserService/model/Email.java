package UserService.UserService.model;

import UserService.UserService.model.enums.Visibility;
import UserService.UserService.model.user.User;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Email implements Serializable {

    private static final long serialVersionUID = 586040086436093034L;

    @Id
    @Column(updatable = false, nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
   //  @OneToOne(cascade = CascadeType.ALL, optional = false)
    @Enumerated(EnumType.STRING)
    private Visibility visibility;

    @Enumerated(EnumType.STRING)
    private Visibility verification;

    @NotNull
    @javax.validation.constraints.Email
    private String email;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(unique = true)
    private User user;

    /**
     * @param email        {@link UserService.UserService.model.user.User} email address

     * @implSpec verification default value {@link UserService.UserService.model.enums.Verification#UNVERIFIED}
     * @implNote default email {@link Visibility} is {@link Visibility#PRIVATE}
     */
    public Email(@javax.validation.constraints.Email @NotNull String email) {
        this.email = email;
        this.visibility = visibility == null ? Visibility.PUBLIC : visibility;
       //  this.verification = verification == null ? Verification.UNVERIFIED : verification;


    }



}
