package UserService.UserService.model.exception;


public abstract class Messages {
    public final static String USER_NOT_FOUND = "Sorry, No user found.";
    public final static String NOT_AN_ACITVE_USER = "Sorry, user is not an active user.";

}
