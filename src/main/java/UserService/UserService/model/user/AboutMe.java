package UserService.UserService.model.user;

import UserService.UserService.model.enums.Status;
import UserService.UserService.model.enums.Visibility;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.swing.plaf.nimbus.State;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@JsonIgnoreProperties(ignoreUnknown = true)
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")

public class AboutMe implements Serializable {

    private static final long serialVersionUID = -1652296521452943989L;

    @Id
    @Column(updatable = false, nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(nullable = false)
    @Size(max = 540)
    private String content;

    @NotNull
    @Column(name = "date_created", updatable = false, nullable = false)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dateAdded;

    @Enumerated(EnumType.STRING)
    private Visibility visibility;

    /**
     * @implNote When user changes their about me, the old becomes {@link Status#INACTIVE}
     * while the new Becomes {@link Status#ACTIVE}.
     */
    @Enumerated(EnumType.STRING)
    private Status status;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", updatable = false, nullable = false)
    private User user;

    /**
     * @param content    User About Me content
     * @param dateAdded  Date About Me is recorded
     * @param visibility {@link User} birthdayVisibility settings (Default: {@link Visibility#PUBLIC})
     * @implSpec state      Current '{@link AboutMe}' {@link Status}.default {@link Status}
     * @implNote When user deletes/Update their AboutMe, the old becomes {@link Status#INACTIVE} the new, {@link Status#ACTIVE}.
     */
    public AboutMe(
            @NotNull @Size(max = 540) String content,
            LocalDateTime dateAdded,
            Visibility visibility
    ) {
        this.visibility = visibility == null ? Visibility.PUBLIC : visibility;
        this.dateAdded = dateAdded;
        this.content = content;
    }
}
