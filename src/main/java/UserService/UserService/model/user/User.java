package UserService.UserService.model.user;

import UserService.UserService.model.Address;
import UserService.UserService.model.Email;
import UserService.UserService.model.Role;
import UserService.UserService.model.education.Education;
import UserService.UserService.model.enums.Gender;
import UserService.UserService.model.enums.Status;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class User implements Serializable {
    private static final long serialVersionUID = 8553136258735824893L;

    @Id
    @Column(updatable = false, nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(name = "user_name" , nullable = false)
    private String  username;

    @Column(name = "password" , nullable = false)
    private String password;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(unique = true)
    private Name name;

    @Enumerated(EnumType.STRING)
    private Gender genders ;

    @Enumerated(EnumType.STRING)
    private Status userStatus;

    @JsonManagedReference
    @Column(unique = true)
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Address> addresses = new HashSet<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(unique = true)
    private Education education;

   @javax.validation.constraints.Email
    private String email;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    @Column(name = "date_created", updatable = false, nullable = false)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dateAdded = LocalDateTime.now();

    // TODO ADD ABOUT ME ENITITY AND JOB

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Column(name = "ABOUT_ME", updatable = false, unique = true)
    private Set<AboutMe> aboutMe = new HashSet<>();

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public void setAddresses(Set<Address> addresses) {
        addresses.forEach(address -> address.setUser(this));
        this.addresses = addresses;
    }

    public void setAboutMe(Set<AboutMe> aboutMes) {
        aboutMes.forEach(aboutMe -> aboutMe.setUser(this));
        this.aboutMe = aboutMes;
    }
}
