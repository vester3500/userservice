package UserService.UserService.model.user;

import UserService.UserService.model.enums.Status;
import UserService.UserService.model.enums.Visibility;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
/*
 * Users are only allowed too change name a certain number of times for now (4)
 * Therefore all previously used names are kept for the user to re-use when they
 * reach the max limit
 */
public class Name implements Serializable {
    private static final long serialVersionUID = -604539590218534363L;
    @Id
    @Column(updatable = false, nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //todo put a pattern match to ensure user ain't putting numerical values.
    @NotNull
    @Size(min = 2, max = 40, message = "first name can not be below two characters")
    @Column(name = "first_name", updatable = false, nullable = false)
    private String firstName;

    @Size( min = 2, max = 40, message = "middle name name can not be below two characters")
    @Column(name = "middle_name", updatable = false)
    private String middleName;

    @NotNull
    @Size(min = 2, max = 40, message = " name can not be below two characters")
    @Column(name = "last_name", updatable = false, nullable = false)
    private String lastName;

    /**
     * @implNote When user changes their name, the old becomes {@link UserService.UserService.model.enums.Status#INACTIVE} while the new,{@link Status#ACTIVE}.
     */
    @Enumerated(EnumType.STRING)
    private Visibility visibility;

    @NotNull
    @Column(name = "DATE_ADDED", updatable = false, nullable = false)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dateAdded;

    /**
     * @param firstName {@link User} First name
     * @param lastName  {@link User} Last name
     * @implNote name {@link Status} by default will be {@link Status#ACTIVE}.
     * Old names MUST be set to {@link Status#INACTIVE}
     */
    public Name(@NotNull String firstName, @NotNull String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.visibility = Visibility.PUBLIC;
        this.dateAdded = LocalDateTime.now();
    }

    /**
     * @param firstName  {@link User} First name
     * @param middleName {@link User} Middle name
     * @param lastName   {@link User} Last name
     * @implNote name {@link Status} by default will be {@link Status#ACTIVE}.
     * Old names MUST be set to {@link Status#INACTIVE}
     */
    public Name(@NotNull String firstName, @NotNull String middleName, @NotNull String lastName) {
        this.firstName = firstName;
        this.middleName = middleName;// @Query(value = "select  from  User  where userStatus = ACTIVE ")
  //  List<User> getAllActiveUser();
        this.lastName = lastName;
        this.visibility = Visibility.PUBLIC;
        this.dateAdded = LocalDateTime.now();
    }
}
