package UserService.UserService.model.enums;

public enum Status {
    ACTIVE, INACTIVE
}
