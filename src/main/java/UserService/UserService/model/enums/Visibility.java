package UserService.UserService.model.enums;


public enum Visibility {
    PUBLIC, PRIVATE
}
