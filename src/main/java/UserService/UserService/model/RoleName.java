package UserService.UserService.model;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}