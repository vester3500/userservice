package UserService.UserService.controller;

import UserService.UserService.model.exception.UserNotFoundException;
import UserService.UserService.model.user.User;
import UserService.UserService.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@RestController
@RequestMapping("api/v1/users")
public class UserController  implements Serializable {

    private static final long serialVersionUID = 8823178024241354655L;

    private final transient UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    /**
     * This method will be used to get all {@link User}
     * @return list of {@link User}
     */
    @GetMapping(produces ="application/json")
    public final List<User> getAllUsers(){ return userService.getAllUsers();}


    /**
     * This method will be used to get a {@link User}
     * @param  {@link User#getId}
     */
    @GetMapping(value = "/{id}", produces ="application/json")
   public final ResponseEntity<User> getUserById(@PathVariable ("id") Long id) throws UserNotFoundException {
        var user = userService.getUserById(id);
        return  ResponseEntity.ok(user);
    }

    /**
     * This method will be used to create a {@link User}
     * @param  {@link User}
     */
    @PostMapping(consumes = "application/json", produces ="application/json")
    ResponseEntity<User> createUser(@RequestBody User user){
        var createdUser = userService.createUser(user);
        return ResponseEntity.status(201).body(createdUser);
    }

    /**
     * This method will be used to delete  {@link User}
     * @param   userId
     * @throws UserNotFoundException When {@link User} is not found to be deleted.
     */
    @DeleteMapping(value = "/{id}")
   public final void deleteUser(@PathVariable ("id") Long userId) throws UserNotFoundException {
        userService.deleteUser(userId); }

    /**
     * This method will be used to update  {@link User}
     * @param   id
     * @param  {@link User}
     * @throws UserNotFoundException When {@link User} is not found to be deleted.
     */
    @PutMapping(value = "/{id}", consumes = "application/json")
    public final void updateUser(@PathVariable ("id") Long id, @RequestBody User user)throws UserNotFoundException
    { userService.updateUser(id, user); }

    /**
     * This method will be used to active a {@link User}
     * @param  {@link User#getId}
     */
    @GetMapping(value = "/{id}/activateUser")
    public final void activateUser(@PathVariable ("id") Long id) throws Exception{
          userService.activateUser(userService.getUserById(id));
    }


    /**
     * This method will be used to deactive a {@link User}
     * @param  {@link User#getId}
     */
    @GetMapping(value = "/{id}/deactivateUser")
   public final void deactivateUser(@PathVariable ("id") Long id) throws Exception{
        userService.deactivateUser(userService.getUserById(id)); }


    /**
     * This method will be used check  {@link User} activeness
     * @param  {@link User#getId}
     */
  @GetMapping(value = "/{id}/checkActiveness")
   public final Boolean isActive(@PathVariable ("id") Long id) {
      return userService.isActive(id);}


    /**
     * This method will be used to get all active {@link User}
     * @return list of active {@link User}
     */
    @GetMapping(value = "/activeUsers",  produces ="application/json")
    public final  List<User> getAllActiveUsers(){
     return userService.getAllActiveUsers();
    }

    /**
     * This method will be used to get {@link User} by username
     * @return {@link User}
     */
    @GetMapping(value = "/getuser/{username}",  produces ="application/json")
    public final User getUserByUserName(@PathVariable ("username") String userName){
        return userService.getUserByUserName(userName);
    }
}
