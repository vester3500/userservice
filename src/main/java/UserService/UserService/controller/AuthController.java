package UserService.UserService.controller;
import UserService.UserService.controller.payload.ApiResponse;
import UserService.UserService.controller.payload.JwtAuthenticationResponse;
import UserService.UserService.controller.payload.LoginRequest;
import UserService.UserService.controller.payload.SignUpRequest;
import UserService.UserService.model.Role;
import UserService.UserService.model.RoleName;
import UserService.UserService.model.exception.AppException;
import UserService.UserService.model.user.User;
import UserService.UserService.repository.RoleRepository;
import UserService.UserService.repository.UserRepo;
import UserService.UserService.security.JwtTokenProvider;
import UserService.UserService.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collections;
// https://www.callicoder.com/spring-boot-spring-security-jwt-mysql-react-app-part-2/
@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {

    private final transient AuthenticationManager authenticationManager;
    private final transient UserService userService;
    private final transient RoleRepository roleRepository;
    private final transient PasswordEncoder passwordEncoder;
    private final transient JwtTokenProvider tokenProvider;
    private final transient UserRepo userRepo;

    @Autowired
    public AuthController(AuthenticationManager authenticationManager, UserService userService, RoleRepository roleRepository, PasswordEncoder passwordEncoder, JwtTokenProvider tokenProvider, UserRepo userRepo) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.tokenProvider = tokenProvider;
        this.userRepo = userRepo;
    }

    @PostMapping( value = "/signin", consumes = "application/json", produces ="application/json")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

    @PostMapping(value = "/signup", consumes = "application/json", produces ="application/json")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        if(userRepo.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity(new AppException("Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        // Creating user's account
        User user = new User( signUpRequest.getUsername(), signUpRequest.getPassword());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new AppException("User Role not set."));

        user.setRoles(Collections.singleton(userRole));

        User result = userService.createUser(user);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/v1/users/{username}")
                .buildAndExpand(result.getUsername()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
       // return ResponseEntity.created(location).body(user);
    }
}
