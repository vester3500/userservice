package UserService.UserService.cache;

import UserService.UserService.model.user.User;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Optional;


public interface UserCacheLocal extends Serializable {

    /**
     * @param userId cache key for the specific {@link User}.
     * @return the {@link User} with the given {@link User#getId()} or {@literal Optional#empty()} if none found.
     */
    @Cacheable(key = "#userId", sync = true)
    Optional<User> getUserOnCacheByUserId(Long userId);


    /**
     * This method should ONLY be used to update and a VALID existing {@link User}.
     *
     * @param user the given {@code {@link User#getId()}} will server as the key for the cache.
     * @return User, and will never be {@literal null}.
     * @implNote returned {@link User} instance for further operations.
     */
    @CachePut(key = "#user.id")
    User saveUserToCache(User user);

    /**
     * @param user this {@link User#getId()} will removed from cache.
     * @implNote should only be used when disabling (deleting a {@link User}).
     * @see UserService.UserService.service.UserService {@code deactivateUser()} for method behavior.
     */
    @Transactional
    @CacheEvict(key = "#user.id")
    default void evictUserFromCache(User user) {
    }
}
