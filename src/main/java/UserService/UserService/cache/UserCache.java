package UserService.UserService.cache;

import UserService.UserService.config.swaggerconfiguration.CacheNames;
import UserService.UserService.model.user.User;
import UserService.UserService.repository.UserRepo;
import lombok.extern.slf4j.Slf4j;
//import org.infinispan.client.hotrod.RemoteCache;
//import org.infinispan.client.hotrod.RemoteCacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

 // using infinispan as our  cache
//@Slf4j
//@Component
//@CacheConfig(cacheNames = CacheNames.USER)
public class UserCache implements Serializable {
    private static final long serialVersionUID = 2598158248118322210L;

//    private transient final UserRepo userRepository;
//    private transient final RemoteCache<Integer, String> cache;
//
//    @Autowired
//    public UserCache(UserRepo userRepository, RemoteCacheManager remoteCacheManager) {
//        this.userRepository = userRepository;
//        cache = remoteCacheManager.administration().getOrCreateCache(CacheNames.USER, "default");
//        try {
//            cache.clearAsync().get(2, TimeUnit.MINUTES);
//        } catch (Exception e) {
//            log.warn(String.format("Unable to clear the cache %s", CacheNames.USER));
//        }
//    }
//
//    @Override
//    public Optional<User> getUserOnCacheByUserId(Long userId) {
//        return userRepository.findById(userId);
//    }
//
//    @Override
//    public User saveUserToCache(User user) {
//        return userRepository.save(user);
//    }
}
