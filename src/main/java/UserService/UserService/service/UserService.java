package UserService.UserService.service;

import UserService.UserService.model.exception.NotAnAcitveUserException;
import UserService.UserService.model.exception.UserNotFoundException;
import UserService.UserService.model.user.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface UserService {


    /**
     * this method should be use to create a new user
     * @param user
     * @return {@link User}
     * @implSpec  add the user to cache before  persisting it to the database.
     */
    User createUser(User user);

    User updateUser(Long id, User user) throws UserNotFoundException;


    void deleteUser(Long userId) throws UserNotFoundException;

    List<User> getAllUsers();

    /**
     * This method will be use to get {@link User} by {code id}.
     *
     * @param userId {@link User#getId} and it should not be null.
     * @return a {@link Optional} of {@link User } type
     */
    User getUserById(Long userId) throws UserNotFoundException;



    boolean isActive(Long id);

    /**
     * This method will be use to active {@link User} by {code id}.
     *
     * @param  {@link User} and it should not be null.
     *
     */
    void activateUser(User user) throws Exception;

    /**
     * This method will be use to deactive {@link User} by {code id}.
     *
     * @param  {@link User} and it should not be null.
     *
     */
    void deactivateUser(User user) throws Exception;

    /**
     * This method will be use to get all  active {@link User}.
     * @return list of {@link User}
     */
    List<User>getAllActiveUsers();

    /**
     * This method will be use to deactive {@link User} by {code id}.
     *
     * @param {@link User} and it should not be null.
     */
     User getUserByUserName(String userName) throws NotAnAcitveUserException;


}
