package UserService.UserService.service;

import UserService.UserService.cache.UserCacheLocal;
import UserService.UserService.model.enums.Status;
import UserService.UserService.model.exception.NotAnAcitveUserException;
import UserService.UserService.model.exception.UserNotFoundException;
import UserService.UserService.model.user.User;
import UserService.UserService.repository.UserRepo;
import com.honerfor.cutils.Que;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static UserService.UserService.model.exception.Messages.NOT_AN_ACITVE_USER;
import static UserService.UserService.model.exception.Messages.USER_NOT_FOUND;

@Slf4j
@Service
public final class UserServiceImp implements UserService {

    private static final long serialVersionUID = 8218173212849087875L;

    private final transient UserRepo userRepository;
    //private  final  transient UserCacheLocal userCacheLocal;

    public UserServiceImp(UserRepo userRepository) {
        this.userRepository = userRepository;
       // this.userCacheLocal = userCacheLocal;
    }

    @Override
    public User createUser(User user) {
        user.setUserStatus(Status.INACTIVE);
       //  userCacheLocal.saveUserToCache(user);
        // TODO: 12/28/19 send email verification email to user's email 
        return userRepository.save(user);
    }

    @Override
    public User getUserById(Long id) {
        return userRepository.findById(id).orElseThrow(()->{
            throw new UserNotFoundException(USER_NOT_FOUND);
        });

    }

    @Override
    public User updateUser(Long userId, User usr) {
        // TODO: 12/18/19 add encryption user password
        var user = this.getUserById(userId);
        user.setUsername(usr.getUsername());
        user.setPassword(usr.getPassword());
        user.setName(usr.getName());
        user.setAddresses(usr.getAddresses());
        user.setEmail(usr.getEmail());

        return  userRepository.save(user);
    }

    @Override
    public void deleteUser(Long userId) {
       Que.run(()-> {
           try {
               this.userRepository.deleteById(userId);
           } catch (UserNotFoundException ex) {
               throw new UserNotFoundException(USER_NOT_FOUND);
           }
       });
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll()
                .parallelStream()
                .filter(user -> user.getUserStatus().equals(Status.ACTIVE))
                .collect(Collectors.toList());
    }

    // todo refactor this method
    @Override
    public boolean isActive(Long id) {
        Optional<User> user = Optional.ofNullable(this.getUserById(id));
        return user.isPresent() && user.get().getUserStatus() == Status.ACTIVE;
    }


    @Override
    public void activateUser(User user) throws Exception {
        Que.run(()-> user.setUserStatus(Status.ACTIVE)).andExecute(()-> this.userRepository.save(user));
    }


    @Override
    public void deactivateUser(User user) throws Exception {
        Que.run(()-> user.setUserStatus(Status.INACTIVE)).andExecute(()-> this.userRepository.save(user));
    }

    @Override
    public List<User> getAllActiveUsers() {
        return null;
    }

    @Override
    public User getUserByUserName(String userName) throws NotAnAcitveUserException {
        var user = userRepository.findByUsernameIgnoreCase(userName);
        if(user.getUserStatus().equals(Status.ACTIVE)) return user;
        throw new NotAnAcitveUserException(NOT_AN_ACITVE_USER);
    }

    // todo   implement can't remember password and change password
    // todo  add search on user use video,  add role and privilege,  add  jwt token document the apis
}
